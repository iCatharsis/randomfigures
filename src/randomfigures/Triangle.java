package randomfigures;

import randomfigures.Figures;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
- Клас Трикутник має особливу властивість - медіана.
*/

class Triangle extends Figures 
{
@Override
public String ReturnOption()
{
iniciate();
double side = Math.sqrt((s/1.732)*4);
double MathematicParametrs =  new BigDecimal(side * (Math.sqrt(3))/2).setScale(3, RoundingMode.HALF_UP).doubleValue();
return "Фігура: Трикутник (рівностонній), Площа = " + s + ", Медіана (бісектриса,висота) = " + MathematicParametrs + ", Колір: " + color;
}
}
