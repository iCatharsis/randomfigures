package randomfigures;

import java.math.BigDecimal;
import java.math.RoundingMode;
import static randomfigures.Figures.r;

/**
- Клас Трапеція має особливу властивість - середня лінія.
*/

class Trapeze extends Figures 
{
@Override
public String ReturnOption()
{
iniciate();
double height = s/(r.nextInt(10)+1);
double MathematicParametrs = new BigDecimal(s/height).setScale(3, RoundingMode.HALF_UP).doubleValue();
return "Фігура: Трапеція, Площа = " + s + ", Середня лінія = " + MathematicParametrs + ", Колір: " + color;
}
}
