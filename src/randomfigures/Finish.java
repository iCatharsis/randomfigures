package randomfigures;

/**
- Клас Finish містить метод main в якиому визначається кількість фігур, котра буде виведена в консоль. 
- Діапазон вказаний від 1 до 9.
- Метод show() від об'єкту RandomFigure.
*/

class Finish 
{
public static void main(String[] arr)
{
RandomFigure RF = new RandomFigure();    
int cnt = RF.cnt;
System.out.println("Кількість фігур в списку: " + cnt);
for(int i = 0; i<cnt; i++)
{
RF.show();
}
} 
}
