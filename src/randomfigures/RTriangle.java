package randomfigures;

import randomfigures.Figures;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
- Клас RTriangle має особливу властивість - гіпотенуза.
*/

class RTriangle extends Figures 
{
@Override
public String ReturnOption()
{
iniciate();
double a = 3.12;
double b = 1.23;
double c = Math.sqrt(a*a+b*b);
double MathematicParametrs =  new BigDecimal(c).setScale(3, RoundingMode.HALF_UP).doubleValue();
return "Фігура: Трикутник (Прямокутний), Площа = " + s + ", Гіпотенуза = " + MathematicParametrs + ", Колір: " + color;
}
}
