package randomfigures;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
- Клас Квадрат має особливу властивість - сторона.
*/

class Square extends Figures 
{
@Override
public String ReturnOption()
{
iniciate();
double MathematicParametrs =  new BigDecimal(Math.sqrt(s)).setScale(3, RoundingMode.HALF_UP).doubleValue();	
return "Фігура: Квадрат, Площа = " + s + ", Сторона = " + MathematicParametrs + ", Колір: " + color;
}
}
