package randomfigures;
 
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
 
/**
- Абстрактний клас Figures містить в собі основні властивості фігур.
- Метод iniciate(), ініціалізує змінну колір та площу для кожної окремої фігури. 
*/
 
abstract class Figures 
{
static Random r = new Random();
String[] arrColor = {"Білий", "Червоний", "Синій"};	
final int cntFigur = r.nextInt(9);
public String color;
public int s; //Площа фігури
void iniciate()
{
this.color = arrColor[r.nextInt(3)];
this.s = r.nextInt(999)+1;
}
String ReturnOption()
{
return "Площа фігури = " + s + ", Колір: " + color;        	
}
}