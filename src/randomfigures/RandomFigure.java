package randomfigures;

import static randomfigures.Figures.r;

/**
- В класі RandomFigure масив selectFigure присвоює метод ReturnOption() кожній фигурі.
- Метод show() обирає випадкове значення з масиву selectFigure і виводить властивість випадкової фігури.
*/

class RandomFigure extends Figures
{
int cnt = cntFigur;
Circle circle = new Circle();
Square square = new Square();
Triangle triangle = new Triangle();
RTriangle rtriangle = new RTriangle();
Trapeze trapeze = new Trapeze(); 
     

public void show(){
Object[] selectFigure = {circle.ReturnOption(),square.ReturnOption(),triangle.ReturnOption(),trapeze.ReturnOption(),rtriangle.ReturnOption()};        	
System.out.println(selectFigure[r.nextInt(5)]);
}
}
