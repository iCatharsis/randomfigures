package randomfigures;

import randomfigures.Figures;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
- Клас круг має особливу функцію - радіус.
- Метод ReturnOption() повертає всі властивості кола.
*/

class Circle extends Figures 
{
public String ReturnOption()
{
iniciate();
double MathematicParametrs = new BigDecimal(Math.sqrt(s/Math.PI)).setScale(3, RoundingMode.HALF_UP).doubleValue();
return "фігура: Коло, Площа = " + s + ", Радіус = " + MathematicParametrs + ", Колір: " + color;	
}
}


